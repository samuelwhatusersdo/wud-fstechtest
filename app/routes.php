<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::pattern('anyUrl', '[a-zA-Z0-9-_=\/]+');

Route::group(array('prefix' => 'api/v1'), function () {
    Route::resource('users', 'UserController', array('except' => array('create', 'edit')));
});

Route::get('/{anyUrl?}', function () {
    return View::make('hello');
});