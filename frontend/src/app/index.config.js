/*@ngInject*/
function config($locationProvider) {
  $locationProvider.html5Mode(true);
}
module.exports = config;
